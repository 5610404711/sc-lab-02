/** �ͺ�Ӷ�� : �繵���÷���С������������¡�� method � class BankAccount 
��� method � class InvestmentFrame ���пѧ��������������ö������� ���ͧ�ҡ�繡�û�С��੾�з��
*/


package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InvestmentTest {
	
	private InvestmentFrame frame;
	private AddInterestListener listener;
	private BankAccount account;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new InvestmentTest();
	}
	
	public InvestmentTest(){
		frame = new InvestmentFrame();
		listener = new AddInterestListener();
		frame.getButton().addActionListener(listener);
		setTestcase();
	}
	
	public void setTestcase(){
		account = new BankAccount(1000);
		frame.getLabel().setText("balance: " + account.getBalance());
	}
	
	class AddInterestListener implements ActionListener
    {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			 double rate = Double.parseDouble(frame.getField().getText());
	         double interest = account.getBalance() * rate / 100;
	         account.deposit(interest);
	         frame.getLabel().setText("balance: " + account.getBalance());
		}
           
    }
    
   

}
